﻿using DevLogs.Core.Interfaces.Services.DocumentService;
using DevLogs.Core.Interfaces.Services.ProjectService;
using DevLogs.Core.Interfaces.Services.TaskService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.Core.Interfaces
{
    public interface IServiceFactory<TDocument, TProject, TTask> : IDisposable
        where TDocument : class
        where TProject : class
        where TTask : class
    {
        IDocumentService<TDocument> DocumentService { get; }
        IProjectService<TProject> ProjectService { get; } 
        ITaskService<TTask> TaskService { get; }
    }
}
