﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.Core.Interfaces.Repository
{
    public interface IRepository<TEntity> : IDisposable where TEntity: class
    {
        Task Create(TEntity entity);

        void Delete(TEntity entity);

        IQueryable<TEntity> Get();

        void Update(TEntity entity);
    }
}
