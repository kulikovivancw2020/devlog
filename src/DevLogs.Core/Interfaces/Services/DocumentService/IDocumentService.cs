﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.Core.Interfaces.Services.DocumentService
{
    public interface IDocumentService<TEntity> : IService<TEntity> where TEntity : class
    {
        List<TEntity> GetByProjectId();
    }
}
