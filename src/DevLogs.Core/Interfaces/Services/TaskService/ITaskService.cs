﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.Core.Interfaces.Services.TaskService
{
    public interface ITaskService<TEntity> : IService<TEntity> where TEntity : class
    {
        List<TEntity> GetByProjectId(int id);
        List<TEntity> GetByUserId(int id);
    }
}
