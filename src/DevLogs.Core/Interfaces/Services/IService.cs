﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.Core.Interfaces.Services
{
    public interface IService<TEntity>  where TEntity : class
    {
        Task Create(TEntity entity);

        Task Delete(TEntity entity);

        Task<List<TEntity>> GetAll();

        TEntity GetById(int id);

        Task Update(TEntity entity);
    }
}
