﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.Core.Interfaces.Services.ProjectService
{
    public interface IProjectService<TEntity> : IService<TEntity> where TEntity : class
    {
        List<TEntity> GetByTaskId(int id);
        List<TEntity> GetByUserId(int id);
        List<TEntity> GetArchive();
    }
}
