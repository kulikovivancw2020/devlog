﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.Core.Interfaces.UnitOfWork
{
    public interface IUnitOfWork
    {
        IRepository<TaskEntity> TaskRepository { get; }

        IRepository<Project> ProjectRepository { get; }

        IRepository<Document> DocumentRepository { get; }

        IRepository<SecurRole> SecurRoleRepository { get; }
        /// <summary>
        /// Save changes in data base.
        /// </summary>
        Task SaveAsync();
    }
}
