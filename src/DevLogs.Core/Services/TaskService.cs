﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces.Services;
using DevLogs.Core.Interfaces.Services.TaskService;
using DevLogs.Core.Interfaces.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.Core.Services
{
    public sealed class TaskService : ITaskService<TaskEntity>
    {
        private readonly IUnitOfWork _unitOfWork;

        public TaskService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task Create(TaskEntity entity)
        {
            if (entity != null)
            {
                await _unitOfWork.TaskRepository.Create(entity);
                await _unitOfWork.SaveAsync();
            }
        }

        public async Task Delete(TaskEntity entity)
        {
            if (entity != null)
            {
                _unitOfWork.TaskRepository.Delete(entity);
                await _unitOfWork.SaveAsync();
            }
        }

        public void Dispose()
        {
            GC.Collect();
        }

        public async Task<List<TaskEntity>> GetAll()
        {
            var tasks =  _unitOfWork.TaskRepository.Get().Include(t=>t.Project).ToList();

            if (tasks != null)
            {
                foreach (var task in tasks)
                {
                    if (task.FinaleDate != null)
                    {
                        var finaleDate = Convert.ToDateTime(task.FinaleDate);

                        if (finaleDate < DateTime.Now)
                        {
                            task.IsOverdue = true;
                            await Update(task);
                        }

                        if ((task.IsOverdue == true) && (finaleDate > DateTime.Now))
                        {
                            task.IsOverdue = false;
                            await Update(task);
                        }
                    }
                }
            }

            return tasks.ToList();
        }

        public TaskEntity GetById(int id)
        {
            return _unitOfWork.TaskRepository.Get().Where(t => t.Id == id).FirstOrDefault();
        }

        public List<TaskEntity> GetByProjectId(int id)
        {
            var tasks = _unitOfWork.TaskRepository.Get().Where(p => p.ProjectId == id).ToList();
            return tasks;
        }

        public List<TaskEntity> GetByUserId(int id)
        {
            throw new NotImplementedException();
        }

        public async Task Update(TaskEntity entity)
        {
            if (entity != null)
            {
                _unitOfWork.TaskRepository.Update(entity);
                await _unitOfWork.SaveAsync();
            }
        }
    }
}
