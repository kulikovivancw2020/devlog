﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces.Services.ProjectService;
using DevLogs.Core.Interfaces.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DevLogs.Core.Services
{
    public sealed class ProjectService : IProjectService<Project>
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProjectService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task Create(Project entity)
        {
            if (entity != null) { }
            await _unitOfWork.ProjectRepository.Create(entity);
            await _unitOfWork.SaveAsync();
        }

        public async Task Delete(Project entity)
        {
            if (entity != null)
            {
                _unitOfWork.ProjectRepository.Delete(entity);
                await _unitOfWork.SaveAsync();
            }
        }

        public void Dispose()
        {
            GC.Collect();
        }

        public async Task<List<Project>> GetAll()
        {
            var projects = _unitOfWork.ProjectRepository.Get()
                .Include(u=>u.Users)
                .Include(t=>t.Tasks)
                .Where(p => p.InArchive == false);

            if (projects != null)
            {
                foreach (var project in projects)
                {
                    if (project.FinaleDate != null)
                    {
                        var finaleDate = Convert.ToDateTime(project.FinaleDate);

                        if (finaleDate >= DateTime.Now)
                        {
                            project.IsOverdue = true;
                            await Update(project);
                        }

                        if ((project.IsOverdue == true) && (finaleDate < DateTime.Now))
                        {
                            project.IsOverdue = false;
                            await Update(project);
                        }
                    }
                }
            }

            return projects.ToList();
        }

        public List<Project> GetArchive()
        {
            return _unitOfWork.ProjectRepository.Get().Where(p => p.InArchive == true).ToList();
        }

        public Project GetById(int id)
        {
            return _unitOfWork.ProjectRepository.Get().Include(u=>u.Users).Include(c=>c.Tasks).Where(p => p.Id == id).FirstOrDefault();
        }

        public List<Project> GetByTaskId(int id)
        {
            throw new NotImplementedException();
        }

        public List<Project> GetByUserId(int id)
        {
            throw new NotImplementedException();
        }

        public async Task Update(Project entity)
        {
            if (entity != null)
            {
                _unitOfWork.ProjectRepository.Update(entity);
                await _unitOfWork.SaveAsync();
            }
        }
    }
}
