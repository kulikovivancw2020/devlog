﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces.Services.DocumentService;
using DevLogs.Core.Interfaces.Services.ProjectService;
using DevLogs.Core.Interfaces.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DevLogs.Core.Services
{
    public class DocumentService : IDocumentService<Document>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IProjectService<Project> _projectService;

        public DocumentService(IUnitOfWork unitOfWork,
            IProjectService<Project> projectService)
        {
            _unitOfWork = unitOfWork;
            _projectService = projectService;
        }

        public async Task Create(Document entity)
        {
            await CreateDocumentAsync(entity);

            var docs = GetAll().Result.Where(d => d.Name == entity.Name).ToList();

            if (docs.Count == 0)
            {
                await _unitOfWork.DocumentRepository.Create(entity);

                await _unitOfWork.SaveAsync();
            }
        }

        public void CSV(Document entity)
        {
            var project = _projectService.GetById(entity.ProjectId);
        }

        public async Task Delete(Document entity)
        {
            File.Delete(entity.Path);
            _unitOfWork.DocumentRepository.Delete(entity);
            await _unitOfWork.SaveAsync();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task<List<Document>> GetAll()
        {
            var documents = _unitOfWork.DocumentRepository.Get().ToListAsync();
            return documents;
        }

        public Document GetById(int id)
        {
            return _unitOfWork.DocumentRepository.Get().Where(c => c.Id == id).FirstOrDefault();

        }

        public List<Document> GetByProjectId()
        {
            throw new NotImplementedException();
        }

        public Task Update(Document entity)
        {
            throw new NotImplementedException();
        }

        public async Task CreateDocumentAsync(Document document)
        {
            string folderPath = document.Project.ShortName;

            var pathToFolder = Path.Combine(folderPath);

            document.Date = DateTime.Now.ToShortDateString();

            var fileName = document.Project.ShortName + " " + document.Date + ".doc";

            document.Name = fileName;

            if (!Directory.Exists(pathToFolder))
            {
                Directory.CreateDirectory(pathToFolder);
            }

            var pathToFile = Path.Combine(pathToFolder, fileName);

            document.Path = pathToFile;

            if (!File.Exists(pathToFile))
            {
                using (var writer = new StreamWriter(pathToFile, false))
                {
                    await writer.WriteAsync("Количество работников: " + document.Project.Users.Count + Environment.NewLine);
                    await writer.WriteAsync("Количество задач: " + document.Project.Tasks.Count + Environment.NewLine);
                    await writer.WriteAsync("Работники" + Environment.NewLine);

                    foreach (var user in document.Project.Users)
                    {
                        await writer.WriteAsync("Имя: " + user.Name + " Фамилия: " + user.Surname + Environment.NewLine);
                    }

                    await writer.WriteAsync("Задачи" + Environment.NewLine);

                    foreach (var task in document.Project.Tasks)
                    {
                        if (task.IsOverdue)
                        {
                            await writer.WriteAsync("Просрочена!!!" + "Название: " + task.Name + "Статус: " + task.Status + Environment.NewLine);

                        }
                        else
                        {
                            await writer.WriteAsync("Название: " + task.Name + "Статус: " + task.Status + Environment.NewLine);
                        }
                    }
                }
            }
            else
            {
                using var writer = new StreamWriter(pathToFile, true);
                await writer.WriteAsync("Проект - " + document.Project.Name + ", " + document.Project.ShortName);
                await writer.WriteAsync("Количество работников: " + document.Project.Users.Count + Environment.NewLine);
                await writer.WriteAsync("Количество задач: " + document.Project.Tasks.Count + Environment.NewLine);
                await writer.WriteAsync("Работники" + Environment.NewLine);

                if (document.Project.Users.Count != 0)
                {
                    foreach (var user in document.Project.Users)
                    {
                        await writer.WriteAsync("Имя: " + user.Name + " Фамилия: " + user.Surname + Environment.NewLine);
                    }
                }

                await writer.WriteAsync("Задачи" + Environment.NewLine);

                if (document.Project.Tasks.Count != 0)
                {
                    foreach (var task in document.Project.Tasks)
                    {
                        if (task.IsOverdue)
                        {
                            await writer.WriteAsync("Просрочена!!!" + "Название: " + task.Name + "Описание: " + task.Description + "Статус: " + task.Status + Environment.NewLine);

                        }
                        else
                        {
                            await writer.WriteAsync("Название: " + task.Name + "Статус: " + "Описание: " + task.Description + task.Status + Environment.NewLine);
                        }
                    }
                }
            }
        }
    }
}
