﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces.Services;
using DevLogs.Core.Interfaces.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.Core.Services
{
    public class SecurRoleService : IService<SecurRole>
    {
        private readonly IUnitOfWork _unitOfWork;

        public SecurRoleService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task Create(SecurRole entity)
        {
            await _unitOfWork.SecurRoleRepository.Create(entity);
            await _unitOfWork.SaveAsync();
        }

        public async Task Delete(SecurRole entity)
        {
            _unitOfWork.SecurRoleRepository.Delete(entity);
            await _unitOfWork.SaveAsync();
        }

        public async  Task<List<SecurRole>> GetAll()
        {
            return await _unitOfWork.SecurRoleRepository.Get().ToListAsync();
        }

        public SecurRole GetById(int id)
        {
            return _unitOfWork.SecurRoleRepository.Get().FirstOrDefault(r => r.Id == id);
        }

        public async Task Update(SecurRole entity)
        {
            _unitOfWork.SecurRoleRepository.Update(entity);
            await _unitOfWork.SaveAsync();
        }
    }
}
