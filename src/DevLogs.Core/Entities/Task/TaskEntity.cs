﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.Core.Entities
{
    public class TaskEntity
    {
        public TaskEntity()
        {
            Users = new List<ApplicationUser>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Status { get; set; }

        public int Time { get; set; }

        public string StartDate { get; set; }

        public string FinaleDate { get; set; }

        public bool IsOverdue { get; set; } = default;
        
        public int ProjectId { get; set; }

        public Project Project { get; set; }

        public List<ApplicationUser> Users { get; set; }
    }
}
