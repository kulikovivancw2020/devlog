﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.Core.Entities
{
    public class Document
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Path { get; set; }

        public string Date { get; set; }
        
        public int ProjectId { get; set; }

        public Project Project { get; set; }
    }
}
