﻿using System.Collections.Generic;

namespace DevLogs.Core.Entities
{
    public class Project
    {
        public Project()
        {
            Tasks = new List<TaskEntity>();

            Users = new List<ApplicationUser>();

            Documents = new List<Document>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public string Description { get; set; }

        public string Customer { get; set; }

        public string StartDate { get; set; }

        public string  FinaleDate { get; set; }

        public string Status { get; set; }

        public bool InArchive { get; set; } = default;

        public bool IsOverdue { get; set; } = default;

        public List<ApplicationUser> Users { get; set; }

        public List<Document> Documents { get; set; }

        public List<TaskEntity> Tasks { get; set; }
    }
}
