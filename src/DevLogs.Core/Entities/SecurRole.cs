﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.Core.Entities
{
    public class SecurRole
    {
        public SecurRole()
        {
            Users = new List<ApplicationUser>();
        }

        public int Id { get; set; }

        public string UserRole { get; set; }

        public string SecurLevel { get; set; }

        public List<ApplicationUser> Users { get; set; }
    }
}
