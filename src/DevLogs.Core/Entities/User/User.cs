﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace DevLogs.Core.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            Tasks = new List<TaskEntity>();

            Projects = new List<Project>();
        }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Age { get; set; }

        public int? SecurRoleId { get; set; }

        public SecurRole SecurRole { get; set; }


        public List<TaskEntity> Tasks { get; set; }

        public List<Project> Projects { get; set; }
    }
}
