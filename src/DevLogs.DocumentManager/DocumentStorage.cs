﻿using System;
using System.Diagnostics;
using System.IO;

namespace DevLogs.DocumentManager
{
    public class DocumentStorage
    {
        //.doc для ворда, .txt для текстового файла блокнота, . xls для excel файла 
        public static async void Direc()
        {
            string txt = "Hi Ivan";
            Console.WriteLine("Start write to file");
            string folderPath = "Test";
            // если перед путем использовать /, то тогда по папка создается в диске С, а если без, то в папке проекта в bin\Debug\net5.0

            var pathToFolder = Path.Combine(folderPath);

            var fileName = "Ivan.doc";//Path.GetRandomFileName(); .docx не работает!

            if (!Directory.Exists(pathToFolder))
            {
                Directory.CreateDirectory(pathToFolder);
            }

            var pathToFile = Path.Combine(pathToFolder, fileName);

            if (!File.Exists(pathToFile))
            {
                using (var writer = new StreamWriter(pathToFile))
                {
                    await writer.WriteAsync(txt);
                }
            }
            else
            {
                using (var writer = new StreamWriter(pathToFile, true))
                {
                    await writer.WriteAsync(txt + Environment.NewLine);
                }
            }
        }
    }
}
