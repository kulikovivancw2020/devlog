﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.Core.Interfaces
{
    public interface IBaseModelFactory<TEntity, TViewModel> : IDisposable where TEntity : class where TViewModel : class
    {
        TEntity Map(TViewModel viewModel);

        TViewModel Map(TEntity entity);

        public List<TViewModel> MapList(IQueryable<TEntity> entity);

        public List<TEntity> MapList(IQueryable<TViewModel> viewModels);
    }
}
