﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces;
using DevLogs.Core.Interfaces.Services.DocumentService;
using DevLogs.Core.Interfaces.Services.ProjectService;
using DevLogs.Core.Interfaces.Services.TaskService;

namespace DevLogs.UI.Factories
{
    public class ServiceFactory:IServiceFactory<Document, Project, TaskEntity>
    {
        IDocumentService<Document> _documentService;
        IProjectService<Project> _projectService;
        ITaskService<TaskEntity> _taskService;

        public ServiceFactory(IDocumentService<Document> documentService,
            IProjectService<Project> projectService,
            ITaskService<TaskEntity> taskService)
        {
            _documentService = documentService;
            _projectService = projectService;
            _taskService = taskService;
        }

        public IDocumentService<Document> DocumentService
        {
            get
            {
                return _documentService;
            }
        }

        public IProjectService<Project> ProjectService
        {
            get
            {
                return _projectService;
            }
        }

        public ITaskService<TaskEntity> TaskService
        {
            get
            {
                return _taskService;
            }
        }

        public void Dispose()
        {
            throw new System.NotImplementedException();
        }
    }
}
