﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces;
using DevLogs.UI.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DevLogs.UI.Factories
{
    public class UserMapFactory : IBaseModelFactory<ApplicationUser, UserViewModel>
    {

        public List<UserViewModel> MapList(IQueryable<ApplicationUser> entity)
        {
            var viewUserList = entity.Select(u => new UserViewModel()
            {
                Name = u.Name,
                Surname = u.Surname,
                CodeName = u.UserName,
                Email = u.Email,
            }).ToList();

            return viewUserList;
        }

        public List<ApplicationUser> MapList(IQueryable<UserViewModel> viewModels)
        {
            var userList = viewModels.Select(u => new ApplicationUser()
            {
                Name = u.Name,
                Surname = u.Surname,
                UserName = u.CodeName,
                Email = u.Email,
            }).ToList();

            return userList;
        }

        public ApplicationUser Map(UserViewModel viewModel)
        {
            var applicationUser = new ApplicationUser()
            {
                Name = viewModel.Name,
                Surname = viewModel.Surname,
                UserName = viewModel.CodeName,
                Email = viewModel.Email,
            };

            return applicationUser;
        }

        public UserViewModel Map(ApplicationUser entity)
        {
            var viewUser = new UserViewModel()
            {
                Name = entity.Name,
                Surname = entity.Surname,
                CodeName = entity.UserName,
                Email = entity.Email,
            };

            return viewUser;
        }

        public void Dispose()
        {
            GC.Collect();
        }
    }
}
