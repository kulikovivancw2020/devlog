﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces;
using DevLogs.UI.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DevLogs.UI.Factories
{
    public class TaskMapFactory : IBaseModelFactory<TaskEntity, TaskViewModel>
    {
        public List<TaskViewModel> MapList(IQueryable<TaskEntity> entity)
        {
            var viewTaskList = entity.Select(t => new TaskViewModel()
            {
                Id = t.Id,
                Name = t.Name,
                Description = t.Description,
                Status = t.Status,
                Time = t.Time,
                Start = t.StartDate,
                FinaleDate = t.FinaleDate
            }).ToList();

            return viewTaskList;
        }

        public List<TaskEntity> MapList(IQueryable<TaskViewModel> viewModels)
        {
            var taskList = viewModels.Select(t => new TaskEntity()
            {
                Id = t.Id,
                Name = t.Name,
                Description = t.Description,
                Status = t.Status,
                Time = t.Time,
                StartDate = t.Start,
                FinaleDate = t.FinaleDate
            }).ToList();

            return taskList;
        }

        public TaskEntity Map(TaskViewModel viewModel)
        {
            var entityTask = new TaskEntity()
            {
                Id = viewModel.Id,
                Name = viewModel.Name,
                Description = viewModel.Description,
                Status = viewModel.Status,
                Time = viewModel.Time,
                StartDate = viewModel.Start,
                FinaleDate = viewModel.FinaleDate
            };

            return entityTask;
        }

        public TaskViewModel Map(TaskEntity entity)
        {
            var viewTask = new TaskViewModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
                Status = entity.Status,
                Time = entity.Time,
                Start = entity.StartDate,
                FinaleDate = entity.FinaleDate
            };

            return viewTask;
        }

        public void Dispose()
        {
            GC.Collect();
        }
    }
}
