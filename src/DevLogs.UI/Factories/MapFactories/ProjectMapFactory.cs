﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces;
using DevLogs.UI.Models;
using DevLogs.UI.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DevLogs.UI.Factories
{
    public class ProjectMapFactory: IBaseModelFactory<Project,ProjectViewModel>
    {
        public Project Map(ProjectViewModel viewModel)
        {
            var entityProject = new Project()
            {
                Id = viewModel.Id,
                Name = viewModel.Name,
                ShortName = viewModel.ShortName,
                Description = viewModel.Description
            };

            return entityProject;
        }

        public ProjectViewModel Map(Project entity)
        {
            var viewProject = new ProjectViewModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                ShortName = entity.ShortName,
                Description = entity.Description
            };

            return viewProject;
        }

        public List<ProjectViewModel> MapList(IQueryable<Project> projects)
        {
            var viewProjectList = projects.Select(p => new ProjectViewModel()
            {
                Id = p.Id,
                Name = p.Name,
                ShortName = p.ShortName,
                Description = p.Description,
            }).ToList();

            return viewProjectList;
        }

        public List<Project> MapList(IQueryable<ProjectViewModel> viewModels)
        {
            var projectList = viewModels.Select(p => new Project()
            {
                Id = p.Id,
                Name = p.Name,
                ShortName = p.ShortName,
                Description = p.Description,
            }).ToList();

            return projectList;
        }

        public void Dispose()
        {
            GC.Collect();
        }
    }
}
