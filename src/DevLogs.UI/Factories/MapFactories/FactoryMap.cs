﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces;
using DevLogs.UI.Factories;
using DevLogs.UI.Models.ViewModels;

namespace DevLogs.UI.Factories
{
    public class MapFactory
    {
        public IBaseModelFactory<Document, DocumentViewModel> DucomentMapFactory
        {
            get
            {
                return new DocumentMapFactory();
            }
        }

        public IBaseModelFactory<Project, ProjectViewModel> ProjectMapFactory
        {
            get
            {
                return new ProjectMapFactory();
            }
        }

        public IBaseModelFactory<TaskEntity, TaskViewModel> TaskMapFactory
        {
            get
            {
                return new TaskMapFactory();
            }
        }
        public IBaseModelFactory<ApplicationUser, UserViewModel> UserMapFactory
        {
            get
            {
                return new UserMapFactory();
            }
        }
    }
}
