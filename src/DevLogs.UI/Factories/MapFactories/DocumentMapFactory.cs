﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces;
using DevLogs.UI.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DevLogs.UI.Factories
{
    public class DocumentMapFactory : IBaseModelFactory<Document, DocumentViewModel>
    {
        public List<DocumentViewModel> MapList(IQueryable<Document> entity)
        {
            var viewDocumentList = entity.Select(d => new DocumentViewModel()
            {
                Id = d.Id,
                Name = d.Name,
                Path = d.Path
            }).ToList();

            return viewDocumentList;
        }

        public List<Document> MapList(IQueryable<DocumentViewModel> viewModels)
        {
            var documentList = viewModels.Select(d => new Document()
            {
                Id = d.Id,
                Name = d.Name,
                Path = d.Path
            }).ToList();

            return documentList;
        }

        public Document Map(DocumentViewModel viewModel)
        {
            var entityDocument = new Document()
            {
                Id = viewModel.Id,
                Name = viewModel.Name,
                Path = viewModel.Path
            };

            return entityDocument;
        }

        public DocumentViewModel Map(Document entity)
        {
            var viewDocument = new DocumentViewModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Path = entity.Path
            };

            return viewDocument;
        }

        public void Dispose()
        {
            GC.Collect();
        }
    }
}
