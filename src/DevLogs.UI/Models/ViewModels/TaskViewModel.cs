﻿using System;
using System.Collections.Generic;

namespace DevLogs.UI.Models.ViewModels
{
    public class TaskViewModel
    {
        public TaskViewModel()
        {
            Users = new List<UserViewModel>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Status { get; set; }

        public int Time { get; set; }

        public string Start { get; set; }

        public string FinaleDate { get; set; }

        public int ProjectId { get; set; }

        public string ProjectShortName { get; set; }

        public List<UserViewModel> Users { get; set; }
    }
}
