﻿using DevLogs.UI.Models.ViewModels;
using System.Collections.Generic;

namespace DevLogs.UI.Models
{
    public class RegisterUser
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string CodeName { get; set; }

        public int RoleId { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public List<RoleViewModel> Roles { get; set; }
    }
}
