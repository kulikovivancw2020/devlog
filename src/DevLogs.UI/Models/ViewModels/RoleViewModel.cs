﻿namespace DevLogs.UI.Models.ViewModels
{
    public class RoleViewModel
    {
        public int Id { get; set; }

        public string UserRole { get; set; }

        public string SecurLevel { get; set; }
    }
}
