﻿using DevLogs.Core.Entities;
using System.Collections.Generic;

namespace DevLogs.UI.Models.ViewModels
{
    public class ProjectViewModel
    {
        public ProjectViewModel()
        {
            Tasks = new List<TaskViewModel>();

            Users = new List<ApplicationUser>();

            Documents = new List<DocumentViewModel>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public string Description { get; set; }

        public List<ApplicationUser> Users { get; set; }

        public List<DocumentViewModel> Documents { get; set; }

        public List<TaskViewModel> Tasks { get; set; }
    }
}
