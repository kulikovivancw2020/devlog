﻿using System.Collections.Generic;

namespace DevLogs.UI.Models.ViewModels
{
    public class UserViewModel
    {
        public UserViewModel()
        {
            Tasks = new List<TaskViewModel>();

            Projects = new List<ProjectViewModel>();
        }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string CodeName { get; set; }

        public string Role { get; set; }

        public string Email { get; set; }

        public List<TaskViewModel> Tasks { get; set; }

        public List<ProjectViewModel> Projects { get; set; }
    }
}
