﻿using DevLogs.Core.Entities;
using System.Collections.Generic;

namespace DevLogs.UI.Models.ViewModels
{
    public class AddUserToProject
    {
        public List<ApplicationUser> Users { get; set; }

        public int ProjectId { get; set; }
    }
}