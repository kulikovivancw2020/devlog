﻿using System;

namespace DevLogs.UI.Models.ViewModels
{
    public class DocumentViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Path { get; set; }

        public DateTime Date { get; set; }
    }
}
