﻿namespace DevLogs.UI.Models.Admin
{
    public class RecoveryPasswordFromAdmin
    {
        public string UserId { get; set; }

        public string NewPassword { get; set; } 
    }
}
