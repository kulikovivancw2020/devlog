﻿using DevLogs.Core.Entities;
using System.Collections.Generic;

namespace DevLogs.UI.Models
{
    public class UpdateUser
    {
        public ApplicationUser User { get; set; }

        public List<SecurRole> Role { get; set; }
    }
}
