﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces.Services;
using DevLogs.Core.Interfaces.Services.ProjectService;
using DevLogs.UI.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DevLogs.UI.Controllers
{
    [Route("project")]
    //[Authorize]
    public class ProjectController : Controller
    {
        private readonly IProjectService<Project> _projectService;
        private readonly UserManager<ApplicationUser> _userManager;

        public ProjectController(IProjectService<Project> projectService, UserManager<ApplicationUser> userManager)
        {
            _projectService = projectService;
            _userManager = userManager;
        }

        [HttpGet]
        [Route("adduser")]
        public IActionResult AddUserToProject(int id)
        {
            var users = _userManager.Users.Where(u => u.Projects.All(p => p.Id != id)).ToList();

            var asignUserModel = new AddUserToProject()
            {
                Users = users,
                ProjectId = id
            };

            return View(asignUserModel);
        }

        [HttpPost]
        [Route("adduser")]
        public async Task<IActionResult> AddUserToProject(string userId, int projectId)
        {
            var user = await _userManager.FindByIdAsync(userId); ;
            var project = _projectService.GetById(projectId);
            user.Projects.Add(project);
            await _projectService.Update(project);
            return RedirectToAction("ProjectMenu", new { Id = projectId });
        }

        [HttpGet]
        [Route("projects")]
        public async Task<IActionResult> Projects(string projectName = null)
        {
            var projectList = await _projectService.GetAll();

            if (projectName != null)
            {
                var projectListWithOneElement = projectList.Where(p => (p.ShortName == projectName) || (p.Name == projectName)).ToList();

                return View(projectListWithOneElement);
            }

            return View(projectList.Where(p => p.InArchive == false).ToList());
        }

        [HttpGet]
        [Route("add")]
        public IActionResult AddProject()
        {
            if(Request.Cookies["secur-lvl"] != "Low")
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [HttpGet]
        [Route("update")]
        public IActionResult UpdateProject(int projectId)
        {
            var project = _projectService.GetById(projectId);

            return View(project);
        }

        [HttpPost]
        [Route("update")]
        public async Task<IActionResult> UpdateProject(Project viewProject)
        {
            if (viewProject.InArchive == false && viewProject.Status == "В архиве")
            {
                viewProject.InArchive = true;
            }

            if (viewProject.InArchive == true && viewProject.Status == "В разработке")
            {
                viewProject.InArchive = false;
            }

            await _projectService.Update(viewProject);

            return RedirectToAction("ProjectMenu", new { Id = viewProject.Id });
        }

        [HttpPost]
        [Route("add")]
        public async Task<IActionResult> AddProject(Project project)
        {
            if (!ModelState.IsValid)
            {
                return View(project);
            }

            await _projectService.Create(project);
            return RedirectToAction("Projects");

        }

        [HttpGet]
        [Route("{id?}")]
        public IActionResult ProjectMenu(int id)
        {
            var project = _projectService.GetById(id);
            return View(project);
        }

        [HttpGet]
        [Route("myprojects")]
        public async Task<IActionResult> MyProjects()
        {
            var userKey = Request.Cookies["user_key"];

            var user = await _userManager.Users.Include(c => c.Projects).FirstOrDefaultAsync(u => u.Id == userKey);

            return View(user.Projects.Where(p => p.InArchive == false).ToList());
        }

        [HttpGet]
        [Route("users/{id?}")]
        public IActionResult ProjectUsers(int id)
        {
            ViewBag.Id = id;

            var users = _userManager.Users.Include(u => u.Projects).ToList();//.Where(p=>p.Projects.All(p => p.Id == id && p.InArchive == false)).ToList();

            var usersPro = _projectService.GetById(id);

            return View(usersPro.Users);
        }

        [HttpPost]
        [Route("deleteuser")]
        public async Task<IActionResult> DeleteUserFromProject(int projectId, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);

            var project = _projectService.GetById(projectId);

            project.Users.Remove(user);

            await _projectService.Update(project);

            return RedirectToAction("ProjectUsers", new { Id = projectId });
        }
    }
}
