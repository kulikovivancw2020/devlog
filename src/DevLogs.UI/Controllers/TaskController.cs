﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces.Services.TaskService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DevLogs.UI.Controllers
{
    [Authorize]
    [Route("task")]
    public class TaskController : Controller
    {
        private readonly ITaskService<TaskEntity> _taskService;
        private readonly UserManager<ApplicationUser> _userManager;
        public TaskController(ITaskService<TaskEntity> taskService, UserManager<ApplicationUser> userManager)
        {
            _taskService = taskService;
            _userManager = userManager;
        }

        [HttpGet]
        [Route("create")]
        public IActionResult CreateNewTask(int id)
        {
            ViewBag.Id = id;

            var task = new TaskEntity()
            {
                ProjectId = id
            };
            return View(task);
        }

        [HttpGet]
        [Route("updatetask")]
        public IActionResult UpdateTask(int taskId)
        {
            var task = _taskService.GetById(taskId);
            return View(task);
        }

        [HttpPost]
        [Route("updatetask")]
        public async Task<IActionResult> UpdateTask(TaskEntity taskEntity)
        {
            var task = _taskService.GetById(taskEntity.Id);
            task.Name = taskEntity.Name;
            task.Description = taskEntity.Description;
            task.Time = taskEntity.Time;
            task.StartDate = taskEntity.StartDate;
            task.FinaleDate = taskEntity.FinaleDate;
            task.Status = taskEntity.Status;

            await _taskService.Update(task);

            return RedirectToAction("ProjectMenu", "Project", new { Id = taskEntity.ProjectId });
        }

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> CreateTask(TaskEntity taskEntity)
        {
            if (!ModelState.IsValid)
            {
                return View(taskEntity);
            }

            taskEntity.Status = "Активно";

            await _taskService.Create(taskEntity);

            return RedirectToAction("ProjectMenu", "Project", new { Id = taskEntity.ProjectId });
        }

        [HttpGet]
        [Route("tasks")]
        public  IActionResult Tasks(int id)
        {
            ViewBag.Id = id;
            var tasks =  _taskService.GetAll().Result.Where(t=>t.ProjectId == id).ToList();
            return View(tasks);
        }

        [HttpGet]
        [Route("addusertotask")]
        public IActionResult AddUserToTask(int taskId, int projectId)
        {
            var users = _userManager.Users.Include(t => t.Tasks.Where(p=>p.Id != taskId && p.ProjectId == projectId)).ToList();

            var addUser = new AddUserToTask()
            {
                taskId = taskId,
                Users = users
            };
            return View(addUser);
        }

        [HttpPost]
        [Route("deletetask")]
        public async Task<IActionResult> DeleteTask(int taskId)
        {
            var task = _taskService.GetById(taskId);

            await _taskService.Delete(task);
            return RedirectToAction("ProjectMenu", "Project", new { Id = task.ProjectId });
        }


        [HttpPost]
        [Route("addusertotask")]
        public async Task<IActionResult> AddUserToTask(int taskId, string userId)
        {
            var task = _taskService.GetById(taskId);
            var user = await _userManager.FindByIdAsync(userId);

            task.Users.Add(user);
            await _taskService.Update(task);
            return RedirectToAction("ProjectMenu", "Project", new { Id = task.ProjectId });
        }
    }

    public class AddUserToTask
    {
        public int taskId { get; set; }

        public List<ApplicationUser> Users { get; set; }
    }
}
