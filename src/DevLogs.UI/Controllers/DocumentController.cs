﻿using ClosedXML.Excel;
using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces.Services;
using DevLogs.Core.Interfaces.Services.ProjectService;
using DevLogs.Core.Interfaces.Services.TaskService;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.UI.Controllers
{
    public class DocumentController : Controller
    {
        private readonly IProjectService<Project> _projectService;
        private readonly IService<Document> _documentService;
        private readonly ITaskService<TaskEntity> _taskService;
        private readonly IService<SecurRole> _securRoleService;

        public DocumentController(IProjectService<Project> projectService,
            IService<Document> documentService,
             ITaskService<TaskEntity> taskService,
            IService<SecurRole> securRoleService
            )
        {
            _projectService = projectService;
            _documentService = documentService;
            _taskService = taskService;
            _securRoleService = securRoleService;
        }

        [HttpPost]
        [Route("deletedocument")]
        public async Task<IActionResult> DeleteDocument(int documentId)
        {
            var document = _documentService.GetById(documentId);
            await _documentService.Delete(document);
            return RedirectToAction("AdministrationDocumentPage", "Admin");
        }

        [HttpPost]
        [Route("createdocument")]
        public async Task<IActionResult> CreateDocument(int projectId)
        {
            var project = _projectService.GetById(projectId);

            Document document = new()
            {
                ProjectId = project.Id,
                Project = project
            };

            await _documentService.Create(document);
            return RedirectToAction("ProjectMenu", "Project", new { Id = projectId });
        }


        [HttpGet]
        [Route("projectdoc")]
        public IActionResult CreateDocReport(int projectId)
        {
            var project = _projectService.GetById(projectId);

            using (var workBook = new XLWorkbook())
            {
                var worksheet = workBook.Worksheets.Add($"Project {project.ShortName}");
                var currentRow = 1;
                worksheet.Cell(currentRow, 1).Value = "Name";
                worksheet.Cell(currentRow, 2).Value = "Shortname";
                worksheet.Cell(currentRow, 3).Value = "Description";
                worksheet.Cell(currentRow, 4).Value = "Time";
                worksheet.Cell(currentRow, 5).Value = "StartDate";
                worksheet.Cell(currentRow, 6).Value = "FinaleDate";
                worksheet.Cell(currentRow, 7).Value = "Status";

                currentRow++;
                worksheet.Cell(currentRow, 1).Value = project.Name;
                worksheet.Cell(currentRow, 2).Value = project.ShortName;
                worksheet.Cell(currentRow, 3).Value = project.Description;
                worksheet.Cell(currentRow, 4).Value = project.Customer;
                worksheet.Cell(currentRow, 5).Value = project.StartDate;
                worksheet.Cell(currentRow, 6).Value = project.FinaleDate;
                worksheet.Cell(currentRow, 7).Value = project.Status;

                ++currentRow;
                ++currentRow;
                worksheet.Cell(currentRow, 1).Value = "Работники";

                currentRow++;
                worksheet.Cell(currentRow, 1).Value = "Name";
                worksheet.Cell(currentRow, 2).Value = "Surname";
                worksheet.Cell(currentRow, 3).Value = "Email";
                worksheet.Cell(currentRow, 4).Value = "Role";

                foreach(var user in project.Users)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = user.Name;
                    worksheet.Cell(currentRow, 2).Value = user.Surname;
                    worksheet.Cell(currentRow, 3).Value = user.Email;
                    worksheet.Cell(currentRow, 4).Value = _securRoleService.GetById((int)user.SecurRoleId).UserRole;
                }

                currentRow++;
                ++currentRow;
                worksheet.Cell(currentRow, 1).Value = "Задачи";

                currentRow++;

                worksheet.Cell(currentRow, 1).Value = "Name";
                worksheet.Cell(currentRow, 2).Value = "Description";
                worksheet.Cell(currentRow, 3).Value = "Status";
                worksheet.Cell(currentRow, 4).Value = "Time";
                worksheet.Cell(currentRow, 5).Value = "StartDate";
                worksheet.Cell(currentRow, 6).Value = "FinaleDate";

                foreach (var task in project.Tasks)
                {
                    if (task.IsOverdue == true)
                    {
                        currentRow++;
                        //worksheet.Cell(currentRow, 1).Value = task.Id;
                        worksheet.Cell(currentRow, 1).Value = task.Name;
                        worksheet.Cell(currentRow, 2).Value = task.Description;
                        worksheet.Cell(currentRow, 3).Value = task.Status;
                        worksheet.Cell(currentRow, 4).Value = task.Time;
                        worksheet.Cell(currentRow, 5).Value = task.StartDate;
                        worksheet.Cell(currentRow, 6).Value = task.FinaleDate;
                    }
                }

                using (var stream = new MemoryStream())
                {
                    workBook.SaveAs(stream);
                    var conect = stream.ToArray();
                    return File(conect, "application/vnd.openxmlformsts-officedocument.spreadsheetml.sheet", $"{project.ShortName}.xlsx");
                }
            }
        }

        [HttpGet]
        [Route("taskdoc")]
        public async Task<IActionResult> CreateTaskReport()
        {
            var tasks = await _taskService.GetAll();

            using (var workBook = new XLWorkbook())
            {
                var worksheet = workBook.Worksheets.Add("Tasks");
                var currentRow = 1;
                //worksheet.Cell(currentRow, 1).Value = "Id";
                worksheet.Cell(currentRow, 1).Value = "Name";
                worksheet.Cell(currentRow, 2).Value = "Description";
                worksheet.Cell(currentRow, 3).Value = "Status";
                worksheet.Cell(currentRow, 4).Value = "Time";
                worksheet.Cell(currentRow, 5).Value = "StartDate";
                worksheet.Cell(currentRow, 6).Value = "FinaleDate";
                worksheet.Cell(currentRow, 7).Value = "Project";

                foreach (var task in tasks)
                {
                    if (task.IsOverdue == true)
                    {
                        currentRow++;
                        //worksheet.Cell(currentRow, 1).Value = task.Id;
                        worksheet.Cell(currentRow, 1).Value = task.Name;
                        worksheet.Cell(currentRow, 2).Value = task.Description;
                        worksheet.Cell(currentRow, 3).Value = task.Status;
                        worksheet.Cell(currentRow, 4).Value = task.Time;
                        worksheet.Cell(currentRow, 5).Value = task.StartDate;
                        worksheet.Cell(currentRow, 6).Value = task.FinaleDate;
                        worksheet.Cell(currentRow, 7).Value = task.Project.ShortName;
                    }
                }

                using (var stream = new MemoryStream())
                {
                    workBook.SaveAs(stream);
                    var conect = stream.ToArray();
                    return File(conect, "application/vnd.openxmlformsts-officedocument.spreadsheetml.sheet", "tasks.xlsx");
                }
            }
        }
    }
}
