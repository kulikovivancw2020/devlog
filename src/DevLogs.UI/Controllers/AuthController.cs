﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces;
using DevLogs.Core.Interfaces.Services;
using DevLogs.UI.Models;
using DevLogs.UI.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DevLogs.UI.Controllers
{
    [Route("auth")]
    [AllowAnonymous]
    public class AuthController : Controller
    {
        #region Fields
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly SignInManager<ApplicationUser> _signInManager;

        private readonly IEmailService _emailService;
        private readonly IService<SecurRole> _securRoleService;

        #endregion Fields

        public AuthController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailService emailService,
            IService<SecurRole> securRoleService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailService = emailService;
            _securRoleService = securRoleService;
        }

        #region Register
        [HttpGet]
        [Route("register")]
        public async Task<IActionResult> Register()
        {
            var registerUser = new RegisterUser();
            var roles = await _securRoleService.GetAll();
            registerUser.Roles = roles.Select(r => new RoleViewModel()
            {
                Id = r.Id,
                UserRole = r.UserRole
            }).ToList();

            return View(registerUser);
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register(RegisterUser registerUser)
        {
            if (!ModelState.IsValid)
            {
                return View(registerUser);
            }

            ApplicationUser user = new()
            {
                Name = registerUser.Name,
                Surname = registerUser.Surname,
                UserName = registerUser.CodeName,
                Email = registerUser.Email,
                SecurRoleId = registerUser.RoleId,
                SecurRole = _securRoleService.GetById(registerUser.RoleId)
            };

            var result = await _userManager.CreateAsync(user, registerUser.Password);

            if (!result.Succeeded)
            {
                return View(registerUser);
            }

            await _emailService.SendEmailAsync(user.Email, "You creditionals", "Login: " + user.Email + " Password: " + registerUser.Password + "\n Your role is" + user.SecurRole.UserRole);

            return RedirectToAction("Index", "Home");
        }
        #endregion Register

        #region Login/Logout
        [HttpGet]
        [Route("login")]
        public IActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        [Route("login")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogIn(RegisterUser userModel)
        {
            if (!ModelState.IsValid)
            {
                return View(userModel);
            }

            var user = await _userManager.FindByEmailAsync(userModel.Email);

            var result = await _userManager.CheckPasswordAsync(user, userModel.Password);

            if (!result)
            {
                return View(userModel);
            }

            await _signInManager.SignInAsync(user, false);
            Response.Cookies.Append("user_key", user.Id);
            Response.Cookies.Append("user_name_varificate", user.UserName);
            Response.Cookies.Append("secur-lvl", _securRoleService.GetById((int)user.SecurRoleId).SecurLevel);
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [Route("logout")]
        [Authorize]
        public async Task<IActionResult> LogOut()
        {
            await _signInManager.SignOutAsync();
            Response.Cookies.Delete("user_name_varificate");
            Response.Cookies.Delete("user_key");
            Response.Cookies.Delete("secur-lvl");
            return RedirectToAction("LogIn");
        }
        #endregion Login/logout

        [HttpGet]
        [Route("recoverypassword")]
        public IActionResult RecoveryPassword(ApplicationUser user)
        {
            return View(user);
        }

        [HttpPost]
        [Route("recoverypassword")]
        public async Task<IActionResult> RecoveryPassword(string id, string oldPassword, string oldPasswordConfim, string newPassword)
        {
            var user = await _userManager.FindByIdAsync(id);

            if (oldPassword != oldPasswordConfim)
            {
                return View(user);
            }

            var resalt = await _signInManager.CheckPasswordSignInAsync(user, oldPassword, false);

            if (!resalt.Succeeded)
            {
                return View(user);
            }

            await _userManager.ChangePasswordAsync(user, oldPassword, newPassword);

            return View();
        }

        [HttpGet]
        [Route("users")]
        public IActionResult Users(string email = null)
        {
            var users = _userManager.Users.ToList();

            if (email != null)
            {
                return View(users.Where(u => u.Email == email).ToList());
            }

            return View(users);
        }

        [HttpGet]
        [Route("delete")]
        public async Task<IActionResult> Delete(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            return View(user);
        }

        [HttpGet]
        [Route("myaccount")]
        public async Task<IActionResult> MyAccount()
        {
            var userKey = Request.Cookies["user_key"];

            if (userKey == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var user = await _userManager.Users.Include(c => c.Projects).Include(c => c.Tasks).Include(r => r.SecurRole).FirstOrDefaultAsync(u => u.Id == userKey);

            return View(user);
        }
    }
}
