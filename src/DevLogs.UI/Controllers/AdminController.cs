﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces.Services;
using DevLogs.Core.Interfaces.Services.ProjectService;
using DevLogs.Core.Interfaces.Services.TaskService;
using DevLogs.UI.Models;
using DevLogs.UI.Models.Admin;
using DevLogs.UI.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DevLogs.UI.Controllers
{
    //[Authorize]
    [Route("admin")]
    public class AdminController : Controller
    {
        private readonly IProjectService<Project> _projectService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IService<SecurRole> _securRoleService;
        private readonly IService<Document> _documentService;
        private readonly ITaskService<TaskEntity> _taskService;

        public AdminController(IProjectService<Project> projectService, UserManager<ApplicationUser> userManager, IService<SecurRole> securRoleService, IService<Document> documentService, ITaskService<TaskEntity> taskService)
        {
            _projectService = projectService;
            _userManager = userManager;
            _securRoleService = securRoleService;
            _documentService = documentService;
            _taskService = taskService;
        }

        #region Roles
        [HttpGet]
        [Route("addrole")]
        public IActionResult AddRole()
        {
            return View();
        }

        [HttpPost]
        [Route("addrole")]
        public async Task<IActionResult> AddRole(RoleViewModel roleViewModel)
        {
            var newRole = new SecurRole()
            {
                UserRole = roleViewModel.UserRole,
                SecurLevel = roleViewModel.SecurLevel
            };

            await _securRoleService.Create(newRole);
            return RedirectToAction("AdministrationRolesPage");
        }

        [HttpGet]
        [Route("adminrolepage")]
        public async Task<IActionResult> AdministrationRolesPage()
        {
            var roles = await _securRoleService.GetAll();

            return View(roles);
        }

        [HttpGet]
        [Route("updaterole")]
        public async Task<IActionResult> UpdateRole(int id)
        {
            var roles = await _securRoleService.GetAll();

            var role = roles.FirstOrDefault(r => r.Id == id);

            var viewRole = new RoleViewModel
            {
                Id = role.Id,
                UserRole = role.UserRole,
                SecurLevel = role.SecurLevel
            };

            return View(viewRole);
        }

        [HttpPost]
        [Route("updaterole")]
        public async Task<IActionResult> UpdateRole(RoleViewModel roleViewModel)
        {
            var role = new SecurRole()
            {
                Id = roleViewModel.Id,
                UserRole = roleViewModel.UserRole,
                SecurLevel = roleViewModel.SecurLevel
            };

            await _securRoleService.Update(role);
            return RedirectToAction("AdministrationRolesPage");
        }

        [HttpPost]
        [Route("deleterole")]
        public async Task<IActionResult> DeleteRole(int id)
        {
            var roles = await _securRoleService.GetAll();
            var role = roles.FirstOrDefault(r => r.Id == id);
            await _securRoleService.Delete(role);
            return RedirectToAction("AdministrationRolesPage");
        }
        #endregion Roles

        #region Documents

        [HttpGet]
        [Route("admindocuments")]
        public async Task<IActionResult> AdministrationDocumentPage()
        {
            var documents = await _documentService.GetAll();

            foreach (var doc in documents)
            {
                doc.Project = _projectService.GetById(doc.ProjectId);
            }

            return View(documents);
        }

        #endregion Documents

        #region Projects

        [HttpGet]
        [Route("adminproject")]
        public async Task<IActionResult> AdministrationProjectPage()
        {
            var pages = await _projectService.GetAll();

            return View(pages);
        }

        [HttpPost]
        [Route("deleteproject")]
        public async Task<IActionResult> DeleteProject(int id)
        {
            var project = _projectService.GetById(id);
            await _projectService.Delete(project);
            return RedirectToAction("AdministrationProjectPage");
        }

        [HttpGet]
        [Route("archive")]
        public IActionResult Archive()
        {
            var projects = _projectService.GetArchive();

            return View(projects);
        }

        [HttpGet]
        [Route("updateproject")]
        public IActionResult UpdateProjectAdmin(int projectId)
        {
            var project = _projectService.GetById(projectId);

            return View(project);
        }

        [HttpPost]
        [Route("updateproject")]
        public async Task<IActionResult> UpdateProjectAdmin(Project viewProject)
        {
            if (viewProject.InArchive == false && viewProject.Status == "В архиве")
            {
                viewProject.InArchive = true;
            }

            if (viewProject.InArchive == true && viewProject.Status == "В разработке")
            {
                viewProject.InArchive = false;
            }

            await _projectService.Update(viewProject);

            return RedirectToAction("AdministrationProjectPage");
        }

        [HttpPost]
        [Route("deletefromarchive")]
        public async Task<IActionResult> DeleteFromArchive(int id)
        {
            var project = _projectService.GetById(id);
            project.InArchive = false;
            await _projectService.Update(project);
            return RedirectToAction("AdministrationProjectPage");
        }

        [HttpGet]
        [Route("projects/warning")]
        public async Task<IActionResult> WarningProjects()
        {
            var projects = await _projectService.GetAll();
            List<Project> list = new List<Project>();
            foreach (var project in projects)
            {
                if ((Convert.ToDateTime(project.FinaleDate) - Convert.ToDateTime(project.StartDate)).Days <= 10)
                {
                    list.Add(project);
                }
            }

            return View(list);
        }
        #endregion Projects

        #region Users
        [HttpGet]
        [Route("users")]
        public IActionResult AdministrationUsersPage()
        {
            var users = _userManager.Users.ToList();
            return View(users);
        }


        [HttpGet]
        [Route("updateuser")]
        public async Task<IActionResult> UpdateUser(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);

            var updateModel = new UpdateUser()
            {
                User = user,
                Role = await _securRoleService.GetAll()
            };

            return View(updateModel);
        }

        [HttpPost]
        [Route("updateuser")]
        public async Task<IActionResult> UpdateUser(UpdateUser updateUser,string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);

            user.Name = updateUser.User.Name;
            user.Surname = updateUser.User.Surname;
            user.UserName = updateUser.User.UserName;
            user.SecurRoleId = updateUser.User.SecurRoleId;
            user.SecurRole = _securRoleService.GetById((int)updateUser.User.SecurRoleId);
            user.Email = updateUser.User.Email;

            await _userManager.UpdateAsync(user);

            return RedirectToAction("AdministrationUsersPage");
        }

        [HttpPost]
        [Route("deleteuser")]
        public async Task<IActionResult> DeleteUser(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            await _userManager.DeleteAsync(user);

            return RedirectToAction("AdministrationUsersPage");
        }

        [HttpGet]
        public IActionResult RecoveryPasswordFromAdmin(string userId)
        {
            var recoveryPasswordModel = new RecoveryPasswordFromAdmin()
            {
                UserId = userId
            };

            return View(recoveryPasswordModel);
        }

        [HttpPost]
        public async Task<IActionResult> RecoveryPasswordFromAdmin(RecoveryPasswordFromAdmin recoveryPasswordFromAdmin)
        {
            var user = await _userManager.FindByIdAsync(recoveryPasswordFromAdmin.UserId);
            await _userManager.RemovePasswordAsync(user);
            await _userManager.AddPasswordAsync(user, recoveryPasswordFromAdmin.NewPassword);
            return View();
        }
        #endregion Users

        [HttpGet]
        [Route("tasks")]
        public async Task<IActionResult> AdministrationTasksPage()
        {
            var users = await _taskService.GetAll();
            return View(users);
        }

        [HttpGet]
        [Route("addusertotask")]
        public IActionResult AddUserToTask(int taskId)
        {
            var task = _taskService.GetById(taskId);
            var users = _userManager.Users.Include(t => t.Tasks.Where(p => p.Id != taskId && p.ProjectId == task.ProjectId)).ToList();

            var addUser = new AddUserToTask()
            {
                taskId = taskId,
                Users = users
            };
            return View(addUser);
        }

        [HttpPost]
        [Route("addusertotask")]
        public async Task<IActionResult> AddUserToTask(int taskId, string userId)
        {
            var task = _taskService.GetById(taskId);
            var user = await _userManager.FindByIdAsync(userId);

            task.Users.Add(user);
            await _taskService.Update(task);
            return RedirectToAction("AdministrationTasksPage");
        }

        [HttpPost]
        [Route("deletetask")]
        public async Task<IActionResult> DeleteTask(int taskId)
        {
            var task = _taskService.GetById(taskId);

            await _taskService.Delete(task);

            return RedirectToAction("AdministrationTasksPage");
        }

        [HttpGet]
        [Route("updatetask")]
        public IActionResult AdminUpdateTask(int taskId)
        {
            var task = _taskService.GetById(taskId);
            return View(task);
        }

        [HttpPost]
        [Route("updatetask")]
        public async Task<IActionResult> AdminUpdateTask(TaskEntity taskEntity)
        {
            var task = _taskService.GetById(taskEntity.Id);
            task.Name = taskEntity.Name;
            task.Description = taskEntity.Description;
            task.Time = taskEntity.Time;
            task.StartDate = taskEntity.StartDate;
            task.FinaleDate = taskEntity.FinaleDate;
            task.Status = taskEntity.Status;

            await _taskService.Update(task);

            return RedirectToAction("AdministrationTasksPage");
        }
    }
}
