using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces;
using DevLogs.Core.Interfaces.Services;
using DevLogs.Core.Interfaces.Services.ProjectService;
using DevLogs.Core.Interfaces.Services.TaskService;
using DevLogs.Core.Interfaces.UnitOfWork;
using DevLogs.Core.Services;
using DevLogs.Ifrustracture.Context;
using DevLogs.Ifrustracture.UnitOfWork;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DevLogs.UI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            services.AddDbContext<ApplicationContext>(options => options
                .UseSqlServer(Configuration
                .GetConnectionString("DataConnection")));
            //opt
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationContext>()
                .AddDefaultTokenProviders();
            services.AddTransient<IUnitOfWork,UnitOfWork>();
            services.AddTransient<ITaskService<TaskEntity>,TaskService>();
            services.AddTransient<IProjectService<Project>, ProjectService>();
            services.AddTransient<IEmailService, EmailService>();
            services.AddTransient<IService<SecurRole>, SecurRoleService>();
            services.AddTransient<IService<Document>, DocumentService>();
            services.AddControllersWithViews();

            services.ConfigureApplicationCookie(cookie => cookie.LoginPath = "/Auth/Login");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            //����� ������ ���� ������������ �������
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
