﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces.Repository;
using DevLogs.Ifrustracture.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.Ifrustracture.Repositories
{
    internal class DocumentRepository : IRepository<Document>
    {
        private readonly ApplicationContext _applicationContext;

        public DocumentRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public async Task Create(Document entity)
        {
            await _applicationContext.Documents.AddAsync(entity);
        }

        public void Delete(Document entity)
        {
            _applicationContext.Documents.Remove(entity);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IQueryable<Document> Get()
        {
            var documents = _applicationContext.Documents.AsQueryable();
            return documents;
        }

        public void Update(Document entity)
        {
            _applicationContext.Documents.Update(entity);
        }
    }
}
