﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces.Repository;
using DevLogs.Ifrustracture.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.Ifrustracture.Repositories
{
    public class TaskRepository : IRepository<TaskEntity>
    {
        private readonly ApplicationContext _applicationContext;

        public TaskRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public async Task Create(TaskEntity entity)
        {
                await _applicationContext.Tasks.AddAsync(entity);
        }

        public void Delete(TaskEntity entity)
        {
            _applicationContext.Tasks.Remove(entity);
        }

        public void Dispose()
        {
            GC.Collect();
        }

        public IQueryable<TaskEntity> Get()
        {
            var tasks = _applicationContext.Tasks.AsQueryable();
            return tasks;
        }

        public void Update(TaskEntity entity)
        {
            _applicationContext.Tasks.Update(entity);
        }
    }
}
