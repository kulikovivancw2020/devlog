﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces.Repository;
using DevLogs.Ifrustracture.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.Ifrustracture.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly ApplicationContext _applicationContext;

        public ProjectRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public async Task Create(Project entity)
        {
            await _applicationContext.Projects.AddAsync(entity);
        }

        public void Delete(Project entity)
        {
            _applicationContext.Projects.Remove(entity);
        }

        public void Dispose()
        {
            GC.Collect();
        }

        public IQueryable<Project> Get()
        {
            var projects = _applicationContext.Projects.AsQueryable();
            return projects;
        }

        public void Update(Project entity)
        {
            _applicationContext.Projects.Update(entity);
        }
    }
}
