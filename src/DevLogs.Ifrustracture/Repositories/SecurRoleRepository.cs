﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces.Repository;
using DevLogs.Ifrustracture.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLogs.Ifrustracture.Repositories
{
    public class SecurRoleRepository : IRepository<SecurRole>
    {
        private readonly ApplicationContext _applicationContext;

        public SecurRoleRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public async Task Create(SecurRole entity)
        {
            await _applicationContext.SecurRoles.AddAsync(entity);
        }

        public void Delete(SecurRole entity)
        {
            _applicationContext.SecurRoles.Remove(entity);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IQueryable<SecurRole> Get()
        {
            return _applicationContext.SecurRoles;
        }

        public void Update(SecurRole entity)
        {
            _applicationContext.SecurRoles.Update(entity);
        }
    }
}
