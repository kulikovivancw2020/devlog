﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DevLogs.Ifrustracture.Migrations
{
    public partial class UpdateProject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Start",
                table: "Projects",
                newName: "StartDate");

            migrationBuilder.AddColumn<bool>(
                name: "InArchive",
                table: "Projects",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InArchive",
                table: "Projects");

            migrationBuilder.RenameColumn(
                name: "StartDate",
                table: "Projects",
                newName: "Start");
        }
    }
}
