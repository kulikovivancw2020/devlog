﻿using DevLogs.Core.Entities;
using DevLogs.Core.Interfaces.Repository;
using DevLogs.Core.Interfaces.UnitOfWork;
using DevLogs.Ifrustracture.Context;
using DevLogs.Ifrustracture.Repositories;
using System.Threading.Tasks;

namespace DevLogs.Ifrustracture.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _applicationContext;
        private IRepository<TaskEntity> _taskRepository;
        private IRepository<Project> _projectRepository;
        private IRepository<Document> _documentRepository;
        private IRepository<SecurRole> _securRoleRepository;

        public UnitOfWork(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public IRepository<TaskEntity> TaskRepository
        {
            get
            {
                if (_taskRepository == null) _taskRepository = new TaskRepository(_applicationContext);
                return _taskRepository;
            }
        }

        public IRepository<Project> ProjectRepository
        {
            get
            {
                if (_projectRepository == null) _projectRepository = new ProjectRepository(_applicationContext);
                return _projectRepository;
            }
        }

        public IRepository<Document> DocumentRepository
        {
            get
            {
                if (_documentRepository == null) _documentRepository = new DocumentRepository(_applicationContext);
                return _documentRepository;
            }
        }

        public IRepository<SecurRole> SecurRoleRepository
        {
            get
            {
                if (_securRoleRepository == null) _securRoleRepository = new SecurRoleRepository(_applicationContext);
                return _securRoleRepository;
            }
        }

        public async Task SaveAsync()
        {
            await _applicationContext.SaveChangesAsync();
        }
    }
}
